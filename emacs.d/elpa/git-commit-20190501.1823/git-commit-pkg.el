;;; -*- no-byte-compile: t -*-
(define-package "git-commit" "20190501.1823" "Edit Git commit messages" '((emacs "25.1") (dash "20180910") (with-editor "20181103")) :commit "faa0e52a6b67280e5114bd87b8958071dbac1af6" :keywords '("git" "tools" "vc") :maintainer '("Jonas Bernoulli" . "jonas@bernoul.li") :url "https://github.com/magit/magit")
